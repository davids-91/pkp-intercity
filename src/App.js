import './App.css';
import { Ticket } from './ticket.js';

const tickets = [
	new Ticket('XX590455622', '11.08.2022', '2h 44min', '13:11', '17:34', '1', '189,00 zł', 'Warszawa Cent.', 'Kraków Gł.', '2620', 'user1')
];

function App() {
	return (
		<div className="container">
			<h1>Moje bilety</h1>
			<nav>
				<a href="/" className="active">Bilety</a>
				<a href="/">Historia podróży</a>
				<a href="/">Zwrócone</a>
			</nav>
			<div className="row space-between">
				<input type="text" placeholder="Szukaj biletu"></input><i className="material-icons search-icon">search</i>
				<button id="filter-btn" className="button-primary">Filtry</button><i className="material-icons filter-icon">insert_chart</i>
			</div>
			<button id="bilet-okresowy-btn"><i className="material-icons">add</i> Dodaj bilet okresowy</button>
			{tickets.map((ticket, index) => (
				<div className="wrapper section" key="{ticket}">
					<div className="row">
						<span className="text-lightblue">Numer biletu</span><span className="text-darkblue">{ticket.id}</span>
						<button className="button-secondary auto-margin">Trasa Twojego pociągu</button>
						<button className="icon-button"><i className="material-icons orange">file_upload</i></button>
						<button className="icon-button google-icon"></button>
					</div>
					<div className="row">
						<div className="wrapper margin-right"><span className="text-lightblue bolded">Data</span><span className="text-darkblue bolded">{ticket.date}</span></div>
						<div className="wrapper margin-right"><span className="text-lightblue bolded">Czas {ticket.time}</span><span className="text-darkblue bolded">{ticket.startHour} <i className="material-icons arrow">chevron_right</i> {ticket.endHour}</span></div>
						<div className="wrapper"><span className="text-lightblue bolded">Klasa {ticket.train_class}</span><span className="text-darkblue bolded">{ticket.price}</span></div>
						<button className="button-primary auto-margin fixed-width orange">Pobierz PDF</button>
					</div>
					<div className="row">
						<div className="wrapper">
							<span className="text-lightblue bolded">Trasa</span><span className="text-darkblue bolded">{ticket.origin} <i className="material-icons arrow">chevron_right</i> {ticket.destination}</span>
							<div className="row"><span className="italic orange bolded text">IC </span><span className="text-darkblue">{ticket.ic}</span></div>
						</div>
						<div className="wrapper auto-margin">
							<button className="button-primary fixed-width orange">Stwórz profil zakupowy</button>
							<button className="button-secondary">Inne funkcje dla tego biletu<i className="material-icons arrow">expand_more</i></button>
						</div>
					</div>
				</div>
			))}
		</div>
	);
}

export default App;
