export function Ticket(id, date, time, startHour, endHour, train_class, price, origin, destination, ic, owner) {
    this.id = id;
    this.date = date;
    this.time = time;
    this.startHour = startHour;
    this.endHour = endHour;
    this.train_class = train_class;
    this.price = price;
    this.origin = origin;
    this.destination = destination;
    this.ic = ic;
    this.owner = owner;
}
